(function() {
    var app = angular.module( 'app', [] );

    app.controller( 'NodesController', [ '$scope', '$http', function( $scope, $http ){

        var node = this;
        $scope = this;

        // Index to count ordered data against all data
        var bucketedItems = 0;
        var itemsToBeAssigned = 0;

        // Initiate arrays needed for bucketSort
        nodes = [];
        nodes[0] = [];
        nodes.nodes = [];

        // Get data from data.json
        this.getData = function(){$http.get('/data.json', {
            headers: {
                'Content-type': 'application/json'
            }
        }).success(function( data ) {

            // Sort data into two main buckets: master nodes and subnodes
            // with subnodes being a temporary container for all unordered subnodes

            for( var i = 0; i < data.length; i++ ) {
                if( data[i].parentId == -1 ) {
                    nodes[0].push( data[i] );
                    bucketedItems += 1;
                    itemsToBeAssigned += 1;
                } else {
                    nodes.nodes.push( data[i] );
                }
            }

            // Calculate remaining items to be assigned
            itemsToBeAssigned = data.length - itemsToBeAssigned;

            // Recursively assign the remaining subnodes to further buckets
            // corresponding to their level
            function assignSubnodesToBuckets( level ) {

                // Break case
                if( data.length === bucketedItems ) {
                    nodes.nodes = [];
                    console.log("All data has been bucketed!");
                    return -1;
                }

                // Initiate new array corresponding to current level
                nodes[level] = [];

                // Recursive case
                if( data.length > bucketedItems ) {

                    for( var j = 0; j < nodes[level - 1].length; j++ ) {

                        for( var k = 0; k < nodes.nodes.length; k++ ) {

                            if( nodes[level - 1][j].id === nodes.nodes[k].parentId ) {
                                nodes[level].push( nodes.nodes[k] );
                                bucketedItems += 1;
                            }
                        }
                    }
                }

                assignSubnodesToBuckets( level + 1 );
            }

            // Start with level 1, since master nodes have been initiated on level 0
            assignSubnodesToBuckets( 1 );


            // Sort data hierarchically in subarrays inside subnodes-property
            function assignSubnodesToParents( subNodeLevel ) {

                // Break case
                if( itemsToBeAssigned === 0 ) {
                    console.log("All subnodes have been assigned to their parents!");

                    // Clean out unneeded buckets
                    for( var n = 1; n < nodes.length; n++ ) {
                        nodes[n] = [];
                    }

                    // Break
                    return -1;
                }

                // Recursive case
                if( itemsToBeAssigned > 0 ) {

                    for( var l = 0; l < nodes[subNodeLevel].length; l++ ) {

                        for( var m = 0; m < nodes[subNodeLevel - 1].length; m++ ) {

                            if( nodes[subNodeLevel][l].parentId === nodes[subNodeLevel - 1][m].id ) {

                                if( !nodes[subNodeLevel - 1][m].nodes ) {
                                    nodes[subNodeLevel - 1][m].nodes = [];
                                    nodes[subNodeLevel - 1][m].nodes.push( nodes[subNodeLevel][l] );
                                } else {
                                    nodes[subNodeLevel - 1][m].nodes.push( nodes[subNodeLevel][l] );
                                }

                                itemsToBeAssigned -= 1;
                            }

                        }
                    }
                }

                assignSubnodesToParents( subNodeLevel + 1 );
            }

            assignSubnodesToParents( 1 );

            nodes.nodes = [];
            $scope.nodes = nodes[0];

        });};

        this.getData();

    }]);
})();

