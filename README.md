# Treehugger

This is just a test project. It's rather quick and dirty.

## Usage:

### 1.

`git clone git@github.com:batoprosic/treehugger.git`

to clone the repository.

____

### 2. 

`grunt dev`

to start the server, compass watch task and open the project in your browser.