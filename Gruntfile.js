module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    compass: {
      dist: {
        options: {
          sassDir: 'styles/scss',
          importPath: 'bower_components/compass-twitter-bootstrap/stylesheets/',
          cssDir: 'styles/css',
          environment: 'development'
        }
      }
    },

    /*uglify: {
      my_target: {
        files: {
          'scripts/build/all.js': ['scripts/libs/*.js']
        }
      }
    },*/

    connect: {
      server: {
        options: {
          port: 9001,
          base: ''
        }
      }
    },

    watch: {
      css: {
        files: 'styles/scss/*.scss',
        tasks: ['compass'],
        options: {
          livereload: true
        }
      },
      html: {
        files: '*.html',
        options: {
          livereload: true
        }
      }
    },

    shell: {
      openbrowser: {
        command: 'open http://localhost:9001/'
      }
    }

  });

  // Load the plugin(s).
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-shell');

  // Default task(s).
  grunt.registerTask('default', ['bower', 'compass', 'uglify']);
  grunt.registerTask('dev', ['connect', 'shell:openbrowser', 'watch']);
};